package com.whodrive.renan.whodrive;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class OpenDetails extends AppCompatActivity {
    private String drivers_name_o_d;
    private String member_in_meeting_o_d;
    private String number_of_cars_o_d;
    private String date_o_d;

    private ImageView photoIv;
    private GoingOut goingOut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_details);

        drivers_name_o_d = getIntent().getStringExtra("drivers_name");
        member_in_meeting_o_d = getIntent().getStringExtra("member_meeting");
        number_of_cars_o_d = getIntent().getStringExtra("number_of_cars");
        date_o_d = getIntent().getStringExtra("date");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            goingOut = (GoingOut)extras.getSerializable("goingOutObject");
        }

        final TextView textView1 = findViewById(R.id.drivers_name_List_o_d);
        final TextView textView2 = findViewById(R.id.number_of_cars_int_o_d);
        final TextView textView3 = findViewById(R.id.member_in_meeting_int_o_d);
        final TextView textView4 = findViewById(R.id.date_o_d);
        photoIv = findViewById(R.id.photo);

        textView1.setText(drivers_name_o_d);
        textView2.setText(number_of_cars_o_d);
        textView3.setText(member_in_meeting_o_d);
        textView4.setText(date_o_d);
        photoIv.setImageBitmap(goingOut.getmPicture());
    }
}
