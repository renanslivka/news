package com.example.renan.shoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddContactActivity extends Activity {

    private EditText firstNameEt;
    private EditText lastNameEt;
    private EditText telephoneEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        firstNameEt = findViewById(R.id.firstname_Et);
        lastNameEt = findViewById(R.id.lastname_Et);
        telephoneEt = findViewById(R.id.telephone_Et);
        Button newContactBtn = findViewById(R.id.add_contact);

        newContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                intent.putExtra(ContactsContract.Intents.Insert.NAME, firstNameEt.getText().toString() + "" + lastNameEt.getText().toString());
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, telephoneEt.getText().toString());
                intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);
                intent.putExtra("fullName",firstNameEt.getText().toString() + lastNameEt.getText().toString());
                intent.putExtra("telephone",telephoneEt.getText().toString());
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                startActivity(intent);
                finish();
            }
        });
    }
}
