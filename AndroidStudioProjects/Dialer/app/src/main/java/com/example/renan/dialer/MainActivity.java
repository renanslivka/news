package com.example.renan.dialer;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends Activity implements View.OnClickListener {
    TextView numberTv;
    final int callPermissionRequest = 1;
    final int SPEECH_RECOGNITION_REQUEST = 2;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numberTv = findViewById(R.id.enter_line);

        if(getIntent().getData()!=null) {
            String number = getIntent().getData().toString();
            number = number.substring(4);
            numberTv.setText(number);
        }

        Button button0 = findViewById(R.id.num0);
        Button button1 = findViewById(R.id.num1);
        Button button2 = findViewById(R.id.num2);
        Button button3 = findViewById(R.id.num3);
        Button button4 = findViewById(R.id.num4);
        Button button5 = findViewById(R.id.num5);
        Button button6 = findViewById(R.id.num6);
        Button button7 = findViewById(R.id.num7);
        Button button8 = findViewById(R.id.num8);
        Button button9 = findViewById(R.id.num9);

        Button starBtn = findViewById(R.id.star_btn);
        ImageButton deleteBtn = findViewById(R.id.delete_btn);
        ImageButton callBtn = findViewById(R.id.call_btn);
        Button sulamitBtn = findViewById(R.id.sulamit);

        button0.setOnClickListener(this);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        starBtn.setOnClickListener(this);
        sulamitBtn.setOnClickListener(this);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String current = numberTv.getText().toString();
                if (current.length() > 0)
                {
                    current = current.substring(0,current.length()-1);
                    numberTv.setText(current);
                }
            }
        });

        deleteBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String current = numberTv.getText().toString();
                if (current.length() > 0)
                {
                    numberTv.setText("");
                }
                return false;
            }
        });
        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT < 23)
                {
                    makeCall();
                }
                else
                {
                    int hasCallPermision = checkSelfPermission(Manifest.permission.CALL_PHONE);
                    if (hasCallPermision == PackageManager.PERMISSION_GRANTED)
                    {
                        makeCall();
                    }
                    else {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},callPermissionRequest);
                    }
                }
            }
        });

        ImageButton micBtn = findViewById(R.id.mic_btn);
        micBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"אמור מספר");
                startActivityForResult(intent,SPEECH_RECOGNITION_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SPEECH_RECOGNITION_REQUEST && resultCode == RESULT_OK)
        {
            Boolean found = false;
            ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            for (int i = 0; i<results.size() && !found; i++)
            {
                String result = results.get(i);;
                if (result.matches("[0-9]+"))
                {
                    numberTv.setText(result);
                    found = true;
                }

            }
            if (!found)
            {
                Toast.makeText(this, "דבר לאט בבקשה!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == callPermissionRequest)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                makeCall();
            }
            else {
                Toast.makeText(this, "Must give permission to call, please go to settings", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void makeCall()
    {
        String number = numberTv.getText().toString();
        Intent intent =new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+number));
        startActivity(intent);
    }
    @Override
    public void onClick(final View v) {
        numberTv.setText(numberTv.getText().toString() + ((Button)v).getText().toString());
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS)
                {
                    tts.speak(((Button)v).getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
                }
            }
        });
    }
}
