package com.homedrill.renan.sportnews.apiNews;

import com.homedrill.renan.sportnews.models.NewsClasses.News;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiNewsInterface {

    @GET("top-headlines")
    Call<News> getNews(
            @Query("country") String country,
            @Query("apiKey") String apiKey);

    @GET("top-headlines")
    Call<News> getGenericNews(
            @Query("country") String country,
            @Query("category") String category,
            @Query("apiKey") String apiKey);

}
