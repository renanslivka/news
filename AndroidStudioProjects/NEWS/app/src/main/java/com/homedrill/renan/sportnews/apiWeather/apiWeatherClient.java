package com.homedrill.renan.sportnews.apiWeather;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class apiWeatherClient {

    public static String BASE_URL = "https://api.openweathermap.org/";
    public static Retrofit retrofit;

    public static Retrofit apiWeatherClient()
    {
        if (retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                   /* .addCallAdapterFactory(RxJava2CallAdapterFactory.create())*/
                    .build();
        }
        return retrofit;
    }
}


