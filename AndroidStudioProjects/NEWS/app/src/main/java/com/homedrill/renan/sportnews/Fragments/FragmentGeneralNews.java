package com.homedrill.renan.sportnews.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.homedrill.renan.sportnews.AdapterNews;
import com.homedrill.renan.sportnews.NewsDetailActivity;
import com.homedrill.renan.sportnews.R;
import com.homedrill.renan.sportnews.apiNews.ApiNewsClient;
import com.homedrill.renan.sportnews.apiNews.ApiNewsInterface;
import com.homedrill.renan.sportnews.models.NewsClasses.Article;
import com.homedrill.renan.sportnews.models.NewsClasses.News;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@SuppressLint("ValidFragment")
public class FragmentGeneralNews extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View fragment_view;
    public static final String API_KEY = "781fe53ce8f74fc48f3e57d70db1088b";
    private List<Article> articles;
    private RecyclerView recyclerView;
    private Context context;
    private AdapterNews adapterNews;
    private SwipeRefreshLayout swipeRefreshLayout;



    @SuppressLint("ValidFragment")
    public FragmentGeneralNews( List<Article> articles,Context context) {
        this.context = context;
        this.articles = articles;
    }

    public void loadJson(){

        swipeRefreshLayout.setRefreshing(true);

        final ApiNewsInterface apiNewsInterface = ApiNewsClient.getApiClient().create(ApiNewsInterface.class);

        //String country = Utils.getCountry();//take the country with the current keyboard
        TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);//take the country with the current connected network
        String country = tm.getNetworkCountryIso();

        Call<News> call = apiNewsInterface.getNews(country,API_KEY);

        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                if (response.isSuccessful()){
                    /*if (!articles.isEmpty() && articles != null){
                        articles.clear();
                    }*/
                    articles = response.body().getArticles();
                    adapterNews = new AdapterNews(articles,context);
                    recyclerView.setAdapter(adapterNews);
                    adapterNews.notifyDataSetChanged();

                    initListener();
                    swipeRefreshLayout.setRefreshing(false);
                }
                else {
                    Toast.makeText(context, "No Result!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void initListener(){
        adapterNews.setOnItemClickListener(new AdapterNews.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(context, NewsDetailActivity.class);

                Article article = articles.get(position);
                intent.putExtra("url", article.getUrl());
                intent.putExtra("title", article.getTitle());
                intent.putExtra("img", article.getUrlToImage());
                intent.putExtra("date", article.getPublishedAt());
                intent.putExtra("source", article.getSource().getName());
                intent.putExtra("author", article.getAuthor());


                startActivity(intent);
            }
        });
    }



    private void initView() {
        recyclerView =  fragment_view.findViewById(R.id.recycler);
        swipeRefreshLayout =  fragment_view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        adapterNews = new AdapterNews( articles,getActivity());
        recyclerView.setAdapter(adapterNews);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        fragment_view = inflater.inflate(R.layout.general_news_fragment, null);
        initView();
        setAdapter();
        loadJson();
        return fragment_view;
    }

    @Override
    public void onRefresh() {
        loadJson();
    }
}
