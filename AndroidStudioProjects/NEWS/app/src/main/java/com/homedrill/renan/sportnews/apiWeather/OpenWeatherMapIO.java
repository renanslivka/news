package com.homedrill.renan.sportnews.apiWeather;

import com.homedrill.renan.sportnews.models.WeatherClasses.WeatherResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenWeatherMapIO {

    @GET("data/2.5/forecast")
    Call<WeatherResult> getWeatherByLatLng(
            @Query("lat") String lat,
            @Query("lon") String lon,
            @Query("appid") String app_id);
}
