package com.homedrill.renan.sportnews.models.NewsClasses;

import com.google.gson.annotations.SerializedName;
import com.homedrill.renan.sportnews.models.NewsClasses.Article;

import java.util.List;

public class News {
    @SerializedName("status")
    private String status;

    @SerializedName("totalResult")
    private int totalResult;

    @SerializedName("articles")
    private List<Article> articles;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResult() {
        return totalResult;
    }

    public void setTotalResult(int totalResult) {
        this.totalResult = totalResult;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
