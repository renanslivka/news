package com.homedrill.renan.sportnews;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class RefreshDataReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.hasExtra("seconds"))
        {
            Intent intent1 = new Intent(context,NewsDetailActivity.class);
            String mUrl = intent.getStringExtra("url");
            String mImg = intent.getStringExtra("img");
            String mTitle = intent.getStringExtra("title");
            String mDate = intent.getStringExtra("date");
            String mSource = intent.getStringExtra("source");
            String mAuthor = intent.getStringExtra("author");
            String mDescription = intent.getStringExtra("description");

            intent1.putExtra("url", mUrl);
            intent1.putExtra("title", mTitle);
            intent1.putExtra("img", mImg);
            intent1.putExtra("date",mDate );
            intent1.putExtra("source",mSource );
            intent1.putExtra("author", mAuthor);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent1,PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_refresh)
                    .setContentTitle(mTitle)
                    .setContentText("there was update of the articles")
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(mDescription))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);
            builder.setDefaults(NotificationCompat.DEFAULT_ALL);

            notificationManager.notify(0,builder.build());
        }


    }

}
