package com.homedrill.renan.sportnews;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import java.util.Timer;
import java.util.TimerTask;

import static com.homedrill.renan.sportnews.RefreshApp.CHANNEL_ID;

public class RefreshService extends IntentService {

    public RefreshService() {
        super(CHANNEL_ID);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int mSeconds = intent.getIntExtra("seconds",10);
        String mUrl = intent.getStringExtra("url");
        String mImg = intent.getStringExtra("img");
        String mTitle = intent.getStringExtra("title");
        String mDate = intent.getStringExtra("date");
        String mSource = intent.getStringExtra("source");
        String mAuthor = intent.getStringExtra("author");

        /*Intent intent1 = new Intent(this,MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent1,0);

        Notification notification = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_refresh)
                .setContentTitle(getWeather().get(0).getTitle())
                .setContentText("there was update of the articles")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(getWeather().get(0).getDescription()))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        startForeground(1,notification);*/




        MyTimerTask myTask = new MyTimerTask();
        Timer myTimer = new Timer();

        myTimer.schedule(myTask, 5000, 10000);

        return START_NOT_STICKY;
    }

    class MyTimerTask extends TimerTask {
        public void run() {
            generateNotification(getApplicationContext(), "Hello");
        }
    }
    private void generateNotification(Context context, String message) {
        long when = System.currentTimeMillis();

        String appname = context.getResources().getString(R.string.app_name);
        NotificationManager notificationManager = (android.app.NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification;
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);

        int icon = R.drawable.ic_refresh;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context);
        notification = builder.setContentIntent(contentIntent)
                .setSmallIcon(icon).setTicker(appname).setWhen(0)
                .setAutoCancel(true).setContentTitle(appname)
                .setContentText(message).build();

        notificationManager.notify((int) when, notification);

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent intent1 = new Intent(this,MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent1,0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis() ,System.currentTimeMillis() + (10*1000), pendingIntent);
    }
}
