package com.homedrill.renan.sportnews;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.homedrill.renan.sportnews.Fragments.FragmentGeneralNews;
import com.homedrill.renan.sportnews.Fragments.FragmentGenericCategory;
import com.homedrill.renan.sportnews.Fragments.FragmentWeather;
import com.homedrill.renan.sportnews.models.NewsClasses.Article;
import com.homedrill.renan.sportnews.models.WeatherClasses.Weather;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.homedrill.renan.sportnews.AdapterNews.getArticles;

public class MainActivity extends AppCompatActivity  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final int RequestPermissionCode = 1;
    protected GoogleApiClient googleApiClient;
    protected TextView longitudeText;
    protected TextView latitudeText;
    protected Location lastLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private List<Article> articles = new ArrayList<>();
    private List<Weather> weathers = new ArrayList<>();

    private AlarmManager alarmManager;
    private PendingIntent actionIntent;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
        private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private StringBuilder builder;
    private FusedLocationProviderClient client;
    private static double lat;
    private static double lon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

/*
        if(Build.VERSION.SDK_INT>=23) {
            int hasLocationPermission = checkSelfPermission(ACCESS_FINE_LOCATION);
            if(hasLocationPermission!= PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{ACCESS_FINE_LOCATION},LOCATION_PERMISSION_REQUEST);
            }
            else startLocation();
        }
        else startLocation();
*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                lat = location.getLatitude();
                                lon = location.getLongitude();
                                tabLayout = findViewById(R.id.tab_layout);
                                viewPager = findViewById(R.id.view_pager);
                                viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
                                viewPagerAdapter.AddFragment(new FragmentGeneralNews(articles,MainActivity.this),"General");//Add fragment General
                                viewPagerAdapter.AddFragment(new FragmentGenericCategory(articles,MainActivity.this,"technology"),"Technology");//Add fragment
                                viewPagerAdapter.AddFragment(new FragmentGenericCategory(articles,MainActivity.this,"sports"),"Sport");//Add fragment
                                viewPagerAdapter.AddFragment(new FragmentGenericCategory(articles,MainActivity.this,"business"),"Business");//Add fragment
                                viewPagerAdapter.AddFragment(new FragmentWeather(MainActivity.this,lat,lon), "Weather");//Add fragment
                                viewPager.setAdapter(viewPagerAdapter);
                                tabLayout.setupWithViewPager(viewPager);

                            }
                        }
                    });
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this, new
                String[]{ACCESS_FINE_LOCATION}, RequestPermissionCode);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("MainActivity", "Connection failed: " + connectionResult.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("MainActivity", "Connection suspendedd");
    }
/*
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == LOCATION_PERMISSION_REQUEST) {
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Sorry can't work", Toast.LENGTH_SHORT).show();
            }
            else startLocation();
        }
    }
*/

/*
    private void startLocation() {

        client = LocationServices.getFusedLocationProviderClient(this);

        LocationCallback callback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                Location lastLocation = locationResult.getLastLocation();

                lat = lastLocation.getLatitude();
                lon = lastLocation.getLongitude();
            }
        };

        LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(1000000);
        request.setFastestInterval(500000);

        if(Build.VERSION.SDK_INT>=23 && checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            client.requestLocationUpdates(request,callback,null);
        else if(Build.VERSION.SDK_INT<=22)
            client.requestLocationUpdates(request,callback,null);


    }

*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.update_every_1_minute:
                pushNotification(60);
                break;

            case R.id.update_every_5_minute:
                pushNotification(300);
                break;

            case R.id.update_every_10_minute:
                pushNotification(600);
                break;

            case R.id.cancel_refresh:
                CancelNotification();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void pushNotification(final int seconds)
    {
        Intent broadcastIntent = new Intent(getApplicationContext(),RefreshDataReceiver.class);
        broadcastIntent.putExtra("seconds",seconds);
        broadcastIntent.putExtra("url", getArticles().get(0).getUrl());
        broadcastIntent.putExtra("title", getArticles().get(0).getTitle());
        broadcastIntent.putExtra("img", getArticles().get(0).getUrlToImage());
        broadcastIntent.putExtra("date",getArticles().get(0).getPublishedAt());
        broadcastIntent.putExtra("source", getArticles().get(0).getSource().getName());
        broadcastIntent.putExtra("author", getArticles().get(0).getAuthor());
        broadcastIntent.putExtra("description", getArticles().get(0).getDescription());

        actionIntent = PendingIntent.getBroadcast(getApplicationContext(),
                0,broadcastIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis() +seconds*1000,
                seconds*1000, actionIntent);
    }

    private void CancelNotification(){
        //Cancel the alarm
        Intent intent = new Intent(this, RefreshDataReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}
