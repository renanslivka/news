package com.homedrill.renan.sportnews.Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.homedrill.renan.sportnews.Function;
import com.homedrill.renan.sportnews.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

@SuppressLint("ValidFragment")
public class FragmentWeather extends Fragment {

    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private double lat;
    private double lon;

    private TextView selectCity, cityField, detailsField, currentTemperatureField,
            humidity_field, pressure_field, weatherIcon, updatedField;
    private ProgressBar loader;
    private Typeface weatherFont;
    private String city = "Hadera, IL";
    /* Please Put your API KEY here */
    private String OPEN_WEATHER_MAP_API = "bfbdb0ea50c12a9ace489c021419b49f";
    /* Please Put your API KEY here */
    private Context context;
    private View fragment_view;
    private boolean isFirstRun = true;

    public FragmentWeather(Context context, double lat, double lon) {
        this.context = context;
        this.lat = lat;
        this.lon = lon;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        fragment_view = inflater.inflate(R.layout.item_weather_cardview, null);
        loader =  fragment_view.findViewById(R.id.loader);
        selectCity =  fragment_view.findViewById(R.id.selectCity);
        cityField = fragment_view. findViewById(R.id.city_field);
        updatedField = fragment_view.findViewById(R.id.updated_field);
        detailsField =  fragment_view.findViewById(R.id.details_field);
        currentTemperatureField = fragment_view. findViewById(R.id.current_temperature_field);
        humidity_field =  fragment_view.findViewById(R.id.humidity_field);
        pressure_field =fragment_view. findViewById(R.id.pressure_field);
        weatherIcon = fragment_view.findViewById(R.id.weather_icon);
        weatherFont = Typeface.createFromAsset(context.getAssets(), "fonts/weathericons-regular-webfont.ttf");
        weatherIcon.setTypeface(weatherFont);
        taskLoadUp(city);
        selectCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Change City");
                final EditText input = new EditText(context);
                input.setText(city);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);

                alertDialog.setPositiveButton("Change",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                city = input.getText().toString();
                                taskLoadUp(city);
                                isFirstRun = false;
                            }
                        });
                alertDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                alertDialog.show();
            }
        });
        return fragment_view;
    }


    public void taskLoadUp(String query) {
        if (Function.isNetworkAvailable(context)) {
            DownloadWeather task = new DownloadWeather();
            task.execute(query);
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }


    class DownloadWeather extends AsyncTask < String, Void, String > {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loader.setVisibility(View.VISIBLE);

        }
        protected String doInBackground(String...args) {
            String xml;
            if (isFirstRun){
                 xml = Function.excuteGet("https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+ lon+
                        "&units=metric&appid=" + OPEN_WEATHER_MAP_API);
            }
            else {
                xml = Function.excuteGet("https://api.openweathermap.org/data/2.5/weather?q=" +args[0] +
                        "&units=metric&appid=" + OPEN_WEATHER_MAP_API);
                isFirstRun = true;
            }
            return xml;
        }
        @Override
        protected void onPostExecute(String xml) {

            try {
                JSONObject json = new JSONObject(xml);
                if (json != null) {
                    JSONObject details = json.getJSONArray("weather").getJSONObject(0);
                    JSONObject main = json.getJSONObject("main");
                    DateFormat df = DateFormat.getDateTimeInstance();

                    cityField.setText(json.getString("name").toUpperCase(Locale.US) + ", " + json.getJSONObject("sys").getString("country"));
                    detailsField.setText(details.getString("description").toUpperCase(Locale.US));
                    currentTemperatureField.setText(String.format("%.2f", main.getDouble("temp")) + "°");
                    humidity_field.setText("Humidity: " + main.getString("humidity") + "%");
                    pressure_field.setText("Pressure: " + main.getString("pressure") + " hPa");
                    updatedField.setText(df.format(new Date(json.getLong("dt") * 1000)));
                    weatherIcon.setText(Html.fromHtml(Function.setWeatherIcon(details.getInt("id"),
                            json.getJSONObject("sys").getLong("sunrise") * 1000,
                            json.getJSONObject("sys").getLong("sunset") * 1000)));

                    loader.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                Toast.makeText(context, "Error, Check City", Toast.LENGTH_SHORT).show();
            }


        }



    }



}
