package com.homedrill.renan.sportnews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.homedrill.renan.sportnews.models.WeatherClasses.Weather;
import com.homedrill.renan.sportnews.models.WeatherClasses.WeatherResult;

import java.util.List;

public class AdapterWeather extends RecyclerView.Adapter <AdapterWeather.MyViewHolder> {

    private static List<Weather> weathers;
    private Context context;
    private com.homedrill.renan.sportnews.AdapterWeather.OnItemClickListener onItemClickListener;
    private WeatherResult weatherResult;

    public static List<Weather> getWeathers() {
        return weathers;
    }

    public AdapterWeather(List<Weather> weathers, Context context) {
        this.weathers = weathers;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterWeather.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_weather_cardview, viewGroup, false);
        return new AdapterWeather.MyViewHolder(view, onItemClickListener);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(@NonNull com.homedrill.renan.sportnews.AdapterWeather.MyViewHolder myViewHolder, int i) {
        final com.homedrill.renan.sportnews.AdapterWeather.MyViewHolder holder = myViewHolder;
        Weather weather = weathers.get(i);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(Utils.getRandomDrawbleColor());
        requestOptions.error(Utils.getRandomDrawbleColor());
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
        requestOptions.centerCrop();

     /*   Glide.with(context)
                .load(weather.getIcon())
                .apply(requestOptions)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.loader.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.loader.setVisibility(View.GONE);
                        return false;
                    }
                })
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.imageView);*/

        //holder.humidity_field.setText("Humidity: " + Integer.parseInt(String.valueOf(Main.getHumidity()))+ "%");


    }


    @Override
    public int getItemCount() {
        return weathers.size();
    }

    public void setOnItemClickListener(com.homedrill.renan.sportnews.AdapterWeather.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final OnItemClickListener onItemClickListener;
        private final TextView detailsField;
        private final TextView currentTemperatureField;
        private ProgressBar loader;
        private TextView selectCity;
        private TextView cityField;
        private TextView updatedField;
        private TextView humidity_field;
        private TextView pressure_field;
        private TextView weatherIcon;


        private MyViewHolder(@NonNull View itemView, com.homedrill.renan.sportnews.AdapterWeather.OnItemClickListener onItemClickListener) {
            super(itemView);

            itemView.setOnClickListener(this);
            loader = itemView.findViewById(R.id.loader);
            selectCity =  itemView.findViewById(R.id.selectCity);
            cityField =itemView.findViewById(R.id.city_field);
            updatedField = itemView.findViewById(R.id.updated_field);
            detailsField = itemView.findViewById(R.id.details_field);
            currentTemperatureField = itemView.findViewById(R.id.current_temperature_field);
            humidity_field = itemView. findViewById(R.id.humidity_field);
            pressure_field = itemView.findViewById(R.id.pressure_field);
            weatherIcon = itemView.findViewById(R.id.weather_icon);


            this.onItemClickListener = onItemClickListener;
        }
        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v,getAdapterPosition());
        }
    }
}