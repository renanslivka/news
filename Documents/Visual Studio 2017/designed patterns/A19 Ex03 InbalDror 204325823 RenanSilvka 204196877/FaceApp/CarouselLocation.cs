﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace FaceApp
{
    public class CarouselLocation 
    {
        private List<CarouselItemLocation> m_Items;
        private WebBrowser m_WebBrowser;
        private int m_SelectedIndex;

        public CarouselLocation(List<CarouselItemLocation> items, WebBrowser i_WebBrowser)
        {
            this.m_Items = items;
            this.m_SelectedIndex = 0;
            this.m_WebBrowser = i_WebBrowser;
            displaySelectedCheckins(this.m_Items[this.m_SelectedIndex]);
        }

        public void DisplayForwardCheckins()
        {
            if (this.m_Items == null || this.m_SelectedIndex < 0)
            {
                return;
            }

            if (this.m_SelectedIndex < this.m_Items.Count - 1)
            {
                this.m_SelectedIndex++;
            }
            else if (this.m_SelectedIndex >= this.m_Items.Count - 1)
            {
                this.m_SelectedIndex = 0;
            }

            displaySelectedCheckins(this.m_Items[this.m_SelectedIndex]);
        }

        public void DisplayPreviousCheckins()
        {
            if (this.m_Items == null || this.m_SelectedIndex < 0)
            {
                return;
            }

            if (this.m_SelectedIndex > 0)
            {
                this.m_SelectedIndex--;
            }
            else if (this.m_SelectedIndex == 0)
            {
                this.m_SelectedIndex = this.m_Items.Count - 1;
            }

            displaySelectedCheckins(this.m_Items[this.m_SelectedIndex]);
        }

        private void displaySelectedCheckins(CarouselItemLocation i_CarouselItemLocation)
        {
            AddressAdapter addressAdapter = new AddressAdapter(i_CarouselItemLocation.m_Place.Location);

            this.m_WebBrowser.Navigate(addressAdapter.m_AddresString);
        }
    }
}