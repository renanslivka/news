﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceApp.Strategy_Class
{
    public static class AgeCalculator
    {
        public static int getAge()
        {
            string DateOfBirthAsString = UserFacebook.getInstance().LoggedInUser.Birthday;
            DateTime DateOfBirth = DateTime.ParseExact(DateOfBirthAsString, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            int YearOfBirth = DateOfBirth.Year;
            return DateTime.Now.Year - YearOfBirth - 1;
        }
    }
}
