﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceApp.Strategy_Class
{
    public class Welcomer
    {
        private IWelcomeStrategy m_Welcomer;
        
        public Welcomer(IWelcomeStrategy i_IWelcomeStrategy)
        {
            this.m_Welcomer = i_IWelcomeStrategy;
        }

        public void SayHi(int age)
        {
            this.m_Welcomer.SayHi(age);
        }
    }
}