﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceApp
{
    public class FormFactory
    {
        private static Assembly s_Assembly = Assembly.GetExecutingAssembly();

        public static Form CreateForm(Type i_Type)
        {
            Form form = s_Assembly.CreateInstance(i_Type.FullName) as Form;
            return form;
        }
    }
}
