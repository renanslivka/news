﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FaceApp;

namespace A16_Ex01_Tom_302986575_Bar_200321412
{
    public class MaleUserVisitor : IVisitor
    {
        private CircularPictureBox m_HeroCircularPictureBox;

        public MaleUserVisitor(CircularPictureBox i_HeroCircularPictureBox)
        {
            this.m_HeroCircularPictureBox = i_HeroCircularPictureBox;
        }

        public void visit()
        {
            m_HeroCircularPictureBox.Image = FaceApp.Properties.Resources.superman;
        }
    }
}
