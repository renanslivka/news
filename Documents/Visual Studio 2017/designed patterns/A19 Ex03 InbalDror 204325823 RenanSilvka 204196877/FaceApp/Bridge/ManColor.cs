﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceApp
{
    public class ManColor : IBridge
    {
        public void ChangeBackColor(Form i_form)
        {
            i_form.BackColor = System.Drawing.Color.LightBlue;
        }
    }
}
