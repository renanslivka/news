﻿using System.Windows.Forms;
using System.Collections.Generic;

namespace FaceApp
{
    public class Carousel
    {
        private List<CarouselItem> m_Items;
        private int m_SelectedIndex;
        public PictureBox m_PictureBox;
        public Label m_LabelName;
        private CarouselLocation m_location;

        public Carousel(PictureBox i_PictureBox, Label i_LabelName, List<CarouselItem> i_Items)
        {
            this.m_Items = i_Items;
            this.m_PictureBox = i_PictureBox;
            this.m_LabelName = i_LabelName;
            this.m_SelectedIndex = 0;
            displaySelected(this.m_Items[this.m_SelectedIndex]);
        }

        public Carousel(PictureBox i_PictureBox, Label i_LabelName, List<CarouselItem> i_Items, CarouselLocation location)
        {
            this.m_Items = i_Items;
            this.m_PictureBox = i_PictureBox;
            this.m_LabelName = i_LabelName;
            this.m_SelectedIndex = 0;
            this.m_location = location;
            displaySelected(this.m_Items[this.m_SelectedIndex]);
        }
        
        public void DisplayForward()
        {
            if (this.m_Items == null || this.m_SelectedIndex < 0)
            {
                return;
            }

            if (this.m_SelectedIndex < this.m_Items.Count - 1)
            {
                this.m_SelectedIndex++;
            }
            else if (this.m_SelectedIndex >= this.m_Items.Count - 1)
            {
                this.m_SelectedIndex = 0;
            }

            if (this.m_location != null)
            {
                this.m_location.DisplayForwardCheckins();
            }

            displaySelected(this.m_Items[this.m_SelectedIndex]);
        }

        public void DisplayPrevious()
        {
            if (this.m_Items == null || this.m_SelectedIndex < 0)
            {
                return;
            }

            if (this.m_SelectedIndex > 0)
            {
                this.m_SelectedIndex--;
            }
            else if (this.m_SelectedIndex == 0)
            {
                this.m_SelectedIndex = this.m_Items.Count - 1;
            }

            if (this.m_location != null)
            {
                this.m_location.DisplayPreviousCheckins();
            }

            displaySelected(this.m_Items[this.m_SelectedIndex]);
        }

        private void displaySelected(CarouselItem i_SelectedItem)
        {
            this.m_PictureBox.ImageLocation = i_SelectedItem.m_ImageUrl;

            if (this.m_LabelName.InvokeRequired)
            {
                this.m_LabelName.BeginInvoke((MethodInvoker)delegate
                {
                    this.m_LabelName.Text = i_SelectedItem.m_Title;
                });
            }
            else
            {
                this.m_LabelName.Text = i_SelectedItem.m_Title;
            }
        }
    }
}
