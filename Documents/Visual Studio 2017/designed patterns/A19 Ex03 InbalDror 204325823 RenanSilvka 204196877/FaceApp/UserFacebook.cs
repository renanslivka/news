﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;
using FacebookWrapper;
using A16_Ex01_Tom_302986575_Bar_200321412;

namespace FaceApp
{
    public class UserFacebook
    {
        private const string k_AppID = "2224083031202871"; //// "510658539406597","317399492389792"; 
        private const string k_GuyAppID = "1450160541956417";
        private static UserFacebook instance;
        private LoginResult m_LoginResult;
        private User m_LoggedInUser;
       //// public ProfilePageForm profilePageForm;

        public static UserFacebook getInstance()
        {
            if (instance == null)
            {
                instance = new UserFacebook();
            }

            return instance;
        }

        public void LoginToFacbook()
        {
            m_LoginResult = FacebookService.Login(
                k_AppID, 
               "public_profile",
               "user_birthday",
               "user_friends",
               "user_events",
               "user_hometown",
               "user_likes",
               "user_location",
               "user_photos",
               "user_posts",
               "user_status",
               "user_tagged_places",
               "user_videos",
               "read_page_mailboxes",
               "manage_pages",
               "publish_pages",
               "email",
               "instagram_basic");

            if (!string.IsNullOrEmpty(m_LoginResult.AccessToken))
            {
                m_LoggedInUser = m_LoginResult.LoggedInUser;                
            }
            else
            {
                throw new Exception(m_LoginResult.ErrorMessage);
            }
        }

        public void Logout(ProfilePageForm p)
        {
            FacebookService.Logout(() => ////anonymos function
            {
                string question = string.Format("\nDo you wish to logut?");
                if (MessageBox.Show(question, "facebook", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    p.Icon = null;
                    p.Text = "logged out !!!";
                    p.Close();
                }
                else
                {
                    ////do nothing!
                }
            });
        }

        public LoginResult LoginResult
        {
            get { return m_LoginResult; } set { }
        }
        
        public User LoggedInUser
        {
            get { return m_LoggedInUser; }
        }
    }   
}
