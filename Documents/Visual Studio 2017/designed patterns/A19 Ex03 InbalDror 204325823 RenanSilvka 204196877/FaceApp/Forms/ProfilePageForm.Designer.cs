﻿namespace FaceApp
{
    public partial class ProfilePageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProfilePageForm));
            this.buttonLogout = new System.Windows.Forms.Button();
            this.buttonShowFriends = new System.Windows.Forms.Button();
            this.buttonShowCheckins = new System.Windows.Forms.Button();
            this.buttonShowPosts = new System.Windows.Forms.Button();
            this.buttonShowAlbum = new System.Windows.Forms.Button();
            this.pictureBoxCover = new System.Windows.Forms.PictureBox();
            this.heroCircularPictureBox = new FaceApp.CircularPictureBox();
            this.circularPictureBoxProfile = new FaceApp.CircularPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCover)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heroCircularPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularPictureBoxProfile)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonLogout
            // 
            this.buttonLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(90)))), ((int)(((byte)(153)))));
            this.buttonLogout.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.buttonLogout.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonLogout.Location = new System.Drawing.Point(526, 320);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(201, 41);
            this.buttonLogout.TabIndex = 13;
            this.buttonLogout.Text = "Logout Facebook";
            this.buttonLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonLogout.UseVisualStyleBackColor = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // buttonShowFriends
            // 
            this.buttonShowFriends.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShowFriends.Image = ((System.Drawing.Image)(resources.GetObject("buttonShowFriends.Image")));
            this.buttonShowFriends.Location = new System.Drawing.Point(216, 167);
            this.buttonShowFriends.Name = "buttonShowFriends";
            this.buttonShowFriends.Size = new System.Drawing.Size(106, 115);
            this.buttonShowFriends.TabIndex = 36;
            this.buttonShowFriends.Text = "Show Friends";
            this.buttonShowFriends.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonShowFriends.UseVisualStyleBackColor = true;
            this.buttonShowFriends.Click += new System.EventHandler(this.buttonShowFriends_Click);
            // 
            // buttonShowCheckins
            // 
            this.buttonShowCheckins.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShowCheckins.Image = global::FaceApp.Properties.Resources.icons8_google_maps_96;
            this.buttonShowCheckins.Location = new System.Drawing.Point(12, 167);
            this.buttonShowCheckins.Name = "buttonShowCheckins";
            this.buttonShowCheckins.Size = new System.Drawing.Size(206, 115);
            this.buttonShowCheckins.TabIndex = 35;
            this.buttonShowCheckins.Text = "Show Checkins";
            this.buttonShowCheckins.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonShowCheckins.UseVisualStyleBackColor = true;
            this.buttonShowCheckins.Click += new System.EventHandler(this.buttonShowCheckins_Click);
            // 
            // buttonShowPosts
            // 
            this.buttonShowPosts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShowPosts.Image = global::FaceApp.Properties.Resources.document;
            this.buttonShowPosts.Location = new System.Drawing.Point(107, 281);
            this.buttonShowPosts.Name = "buttonShowPosts";
            this.buttonShowPosts.Size = new System.Drawing.Size(215, 96);
            this.buttonShowPosts.TabIndex = 34;
            this.buttonShowPosts.Text = "Show Posts";
            this.buttonShowPosts.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonShowPosts.UseVisualStyleBackColor = true;
            this.buttonShowPosts.Click += new System.EventHandler(this.buttonShowPosts_Click);
            // 
            // buttonShowAlbum
            // 
            this.buttonShowAlbum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShowAlbum.Image = global::FaceApp.Properties.Resources.gallery;
            this.buttonShowAlbum.Location = new System.Drawing.Point(12, 281);
            this.buttonShowAlbum.Name = "buttonShowAlbum";
            this.buttonShowAlbum.Size = new System.Drawing.Size(98, 96);
            this.buttonShowAlbum.TabIndex = 33;
            this.buttonShowAlbum.Text = "Show Album";
            this.buttonShowAlbum.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonShowAlbum.UseVisualStyleBackColor = true;
            this.buttonShowAlbum.Click += new System.EventHandler(this.buttonShowAlbum_Click);
            // 
            // pictureBoxCover
            // 
            this.pictureBoxCover.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxCover.Image = global::FaceApp.Properties.Resources._12974543_10205295749907460_5938954788137775313_n;
            this.pictureBoxCover.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCover.Name = "pictureBoxCover";
            this.pictureBoxCover.Size = new System.Drawing.Size(739, 149);
            this.pictureBoxCover.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCover.TabIndex = 32;
            this.pictureBoxCover.TabStop = false;
            // 
            // heroCircularPictureBox
            // 
            this.heroCircularPictureBox.Location = new System.Drawing.Point(328, 169);
            this.heroCircularPictureBox.Name = "heroCircularPictureBox";
            this.heroCircularPictureBox.Size = new System.Drawing.Size(192, 156);
            this.heroCircularPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.heroCircularPictureBox.TabIndex = 38;
            this.heroCircularPictureBox.TabStop = false;
            // 
            // circularPictureBoxProfile
            // 
            this.circularPictureBoxProfile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.circularPictureBoxProfile.Location = new System.Drawing.Point(544, 87);
            this.circularPictureBoxProfile.Name = "circularPictureBoxProfile";
            this.circularPictureBoxProfile.Size = new System.Drawing.Size(183, 179);
            this.circularPictureBoxProfile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.circularPictureBoxProfile.TabIndex = 37;
            this.circularPictureBoxProfile.TabStop = false;
            // 
            // ProfilePageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 396);
            this.Controls.Add(this.heroCircularPictureBox);
            this.Controls.Add(this.circularPictureBoxProfile);
            this.Controls.Add(this.buttonShowFriends);
            this.Controls.Add(this.buttonShowCheckins);
            this.Controls.Add(this.buttonShowPosts);
            this.Controls.Add(this.buttonShowAlbum);
            this.Controls.Add(this.pictureBoxCover);
            this.Controls.Add(this.buttonLogout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProfilePageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ProfilePage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCover)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heroCircularPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularPictureBoxProfile)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.PictureBox pictureBoxCover;
        private System.Windows.Forms.Button buttonShowAlbum;
        private System.Windows.Forms.Button buttonShowPosts;
        private System.Windows.Forms.Button buttonShowCheckins;
        private System.Windows.Forms.Button buttonShowFriends;
        private CircularPictureBox circularPictureBoxProfile;
        private CircularPictureBox heroCircularPictureBox;
    }
}