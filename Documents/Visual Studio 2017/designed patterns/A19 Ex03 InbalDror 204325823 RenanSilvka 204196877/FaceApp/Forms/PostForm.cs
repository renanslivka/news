﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;

namespace FaceApp
{
    public partial class PostForm : Form 
    {
        private UserFacebook m_UserFacebook;
        private WindowSettings m_WindowSettings;
        private Carousel m_Carousel;
        private List<Post> m_Items;
        public IBridge Bridge;

        public PostForm()
        {
            InitializeComponent();
            this.m_UserFacebook = UserFacebook.getInstance();
            this.m_WindowSettings = WindowSettings.getInstance();
            this.m_Items = new List<Post>();
            m_WindowSettings.SetPostsPage(this);
        }
        
        private List<CarouselItem> prepareItems(List<Post> list)
        {
            List<CarouselItem> c_items = new List<CarouselItem>();
            foreach (Post item in list)
            {
                CarouselItem c_item = new CarouselItem(item.Message, item.Caption, item.PictureURL);
                c_items.Add(c_item);
            }

            return c_items;
        }

        private void fetchPosts()
        {
            foreach (Post post in this.m_UserFacebook.LoginResult.LoggedInUser.Posts)
            {
                this.m_Items.Add(post);
            }

            if (this.m_UserFacebook.LoginResult.LoggedInUser.Posts.Count == 0)
            {
                MessageBox.Show("No Posts to retrieve :(");
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            m_Carousel.DisplayPrevious();
        }

        private void buttonForward_Click(object sender, EventArgs e)
        {
            m_Carousel.DisplayForward();
        }

        private void buttonBackToProfilePage_Click(object sender, EventArgs e)
        {
            this.Hide();
            ProfilePageForm window = new ProfilePageForm();
            window.ShowDialog();
        }

        private void PostPage_Load(object sender, EventArgs e)
        {
            if(m_UserFacebook.LoggedInUser.Gender.ToString() == "male")
            {
                Bridge = new ManColor();
                Bridge.ChangeBackColor(this); 
            }
            else
            {
                Bridge = new WomanColor();
                Bridge.ChangeBackColor(this);
            }

            fetchPosts();
            m_Carousel = new Carousel(this.pictureBox, this.labelName, this.prepareItems(this.m_Items));
        }
    }
}
