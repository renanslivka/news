﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;

namespace FaceApp
{
    public partial class CheckinsPage : Form
    {
        private readonly UserFacebook r_UserFacebook;
        private WindowSettings m_WindowSettings;
        private Carousel m_Carousel;
        private List<Checkin> m_Items;
        public IBridge Bridge;

        public CheckinsPage()
        {
            InitializeComponent();
            this.r_UserFacebook = UserFacebook.getInstance();
            this.m_WindowSettings = WindowSettings.getInstance();
            this.m_Items = new List<Checkin>();
            m_WindowSettings.SetCheckinsPage(this);
        }

        private List<CarouselItem> prepareItems(List<Checkin> list)
        {
            List<CarouselItem> listOfItems = new List<CarouselItem>();
            foreach (Checkin item in list)
            {
                CarouselItem c_item = new CarouselItem(item.Message, item.Caption, item.PictureURL);
                listOfItems.Add(c_item);
            }

            return listOfItems;
        }

        private List<CarouselItemLocation> prepareItemsLocation(List<Checkin> list)
        {
            List<CarouselItemLocation> listItemsLocation = new List<CarouselItemLocation>();
            foreach (Checkin item in list)
            {
                CarouselItemLocation ItemLocation = new CarouselItemLocation(item.Place, item.Place.Location.Latitude.ToString(), item.Place.Location.Longitude.ToString());
                listItemsLocation.Add(ItemLocation);
            }

            return listItemsLocation;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            m_Carousel.DisplayPrevious();
        }

        private void buttonForward_Click(object sender, EventArgs e)
        {
            m_Carousel.DisplayForward();
        }

        private void buttonBackToProfilePage_Click(object sender, EventArgs e)
        {
            this.Hide();
            ProfilePageForm window = new ProfilePageForm();
            window.ShowDialog();
            this.Close();
        }

        private void CheckinsPage_Load(object sender, EventArgs e)
        {
            if (r_UserFacebook.LoggedInUser.Gender.ToString() == "male")
            {
                Bridge = new ManColor();
                Bridge.ChangeBackColor(this);
            }
            else
            {
                Bridge = new WomanColor();
                Bridge.ChangeBackColor(this);
            }

            new Thread(fetchCheckins).Start();  
        }

        private void fetchCheckins()
        {
            foreach (Checkin checkin in this.r_UserFacebook.LoggedInUser.Checkins)
            {
                Invoke((MethodInvoker)(() =>
                    this.m_Items.Add(checkin)
                ));
            }

            if (this.r_UserFacebook.LoggedInUser.Checkins.Count == 0)
            {
                MessageBox.Show("No Checkins to retrieve :(");
            }
            else
            {
                m_Carousel = new Carousel(this.pictureBox, this.labelName, this.prepareItems(this.m_Items), new CarouselLocation(this.prepareItemsLocation(this.m_Items), this.webBrowser1));
            }
        }
    }
}
