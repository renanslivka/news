﻿namespace FaceApp
{
    public partial class CheckinsPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.buttonBackToProfilePage = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.buttonForward = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelName.Location = new System.Drawing.Point(37, 28);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(51, 20);
            this.labelName.TabIndex = 48;
            this.labelName.Text = "label2";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(274, 69);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(656, 373);
            this.webBrowser1.TabIndex = 51;
            // 
            // buttonBackToProfilePage
            // 
            this.buttonBackToProfilePage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBackToProfilePage.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.buttonBackToProfilePage.Image = global::FaceApp.Properties.Resources.iconfinder_Undo_27885;
            this.buttonBackToProfilePage.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.buttonBackToProfilePage.Location = new System.Drawing.Point(891, 12);
            this.buttonBackToProfilePage.Name = "buttonBackToProfilePage";
            this.buttonBackToProfilePage.Size = new System.Drawing.Size(52, 31);
            this.buttonBackToProfilePage.TabIndex = 50;
            this.buttonBackToProfilePage.Text = "back";
            this.buttonBackToProfilePage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonBackToProfilePage.UseVisualStyleBackColor = true;
            this.buttonBackToProfilePage.Click += new System.EventHandler(this.buttonBackToProfilePage_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.AutoSize = true;
            this.buttonBack.Image = global::FaceApp.Properties.Resources.back_23;
            this.buttonBack.Location = new System.Drawing.Point(23, 262);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(84, 39);
            this.buttonBack.TabIndex = 49;
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox.Location = new System.Drawing.Point(23, 89);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(196, 167);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox.TabIndex = 47;
            this.pictureBox.TabStop = false;
            // 
            // buttonForward
            // 
            this.buttonForward.AutoSize = true;
            this.buttonForward.Image = global::FaceApp.Properties.Resources.forward_2;
            this.buttonForward.Location = new System.Drawing.Point(135, 262);
            this.buttonForward.Name = "buttonForward";
            this.buttonForward.Size = new System.Drawing.Size(84, 39);
            this.buttonForward.TabIndex = 46;
            this.buttonForward.UseVisualStyleBackColor = true;
            this.buttonForward.Click += new System.EventHandler(this.buttonForward_Click);
            // 
            // CheckinsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 485);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.buttonBackToProfilePage);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.buttonForward);
            this.Name = "CheckinsPage";
            this.Text = "CheckinsPage";
            this.Load += new System.EventHandler(this.CheckinsPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonBackToProfilePage;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button buttonForward;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}