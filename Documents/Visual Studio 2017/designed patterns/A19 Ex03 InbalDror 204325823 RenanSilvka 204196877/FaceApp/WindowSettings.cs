﻿using System;
using System.Windows.Forms;
using FaceApp.Properties;

namespace FaceApp
{
    public class WindowSettings
    {
        private static WindowSettings instance;
        private string m_AccessToken;
        private LoginForm m_LoginPage;
        private ProfilePageForm m_Profilepage;
        private AlbumForm m_AlbumPage;
        private PostForm m_PostsPage;
        private CheckinsPage m_CheckinsPage;
        private FriendsForm m_FriendsPage;

        public static WindowSettings getInstance()
        {
            if (instance == null)
            {
                instance = new WindowSettings();
            }

            return instance;
        }

        public WindowSettings()
        {
            this.m_LoginPage = null;
            this.m_AccessToken = null;
            this.m_Profilepage = null;
        }

        public void setLoginPage(LoginForm O_LoginPage)
        {
            this.m_LoginPage = O_LoginPage;
        }

        public void setAccessToken(string O_AccessToken)
        {
            this.m_AccessToken = O_AccessToken;
        }

        public void setProfilePage(ProfilePageForm O_Profilepage)
        {
            this.m_Profilepage = O_Profilepage;
        }

        public void SetAlbumPage(AlbumForm O_AlbumePage)
        {
            this.m_AlbumPage = O_AlbumePage;
        }

        public void SetPostsPage(PostForm O_PostsPage)
        {
            this.m_PostsPage = O_PostsPage;
        }

        public void SetCheckinsPage(CheckinsPage O_CheckinsPage)
        {
            this.m_CheckinsPage = O_CheckinsPage;
        }

        public void SetFriendsPage(FriendsForm o_FriendsPage)
        {
            this.m_FriendsPage = o_FriendsPage;
        }

        public void RememberSettingBeforeClosingWindow(Form io_Window)
        {
            if (io_Window is LoginForm)
            {
                updateSettingLoginPage();
            }
            else if (io_Window is ProfilePageForm)
            {
                updateSettingProfilePage();
            }
        }

        private void updateSettingLoginPage()
        {
            Settings.Default.CheckBoxLoginPage = m_LoginPage.CheckBoxRememberMe.Checked;
            Settings.Default.LastLoginPageLocationX = m_LoginPage.Location.X;
            Settings.Default.LastLoginPageLocationY = m_LoginPage.Location.Y;
            Settings.Default.LastLoginPageSizeWidth = m_LoginPage.Size.Width;
            Settings.Default.LastLoginPageSizeHeight = m_LoginPage.Size.Height;

            if (m_LoginPage.CheckBoxRememberMe.Checked)
            {
                Settings.Default.LastAccessToken = this.m_AccessToken;
            }
            else
            {
                Settings.Default.LastAccessToken = null;
            }

            Settings.Default.Save();
        }

        private void updateSettingProfilePage()
        {            
            Settings.Default.LastProfilePageLocationX = m_Profilepage.Location.X;
            Settings.Default.LastProfilePageLocationY = m_Profilepage.Location.Y;
            Settings.Default.LastProfilePageSizeWidth = m_Profilepage.Size.Width;
            Settings.Default.LastProfilePageSizeHeight = m_Profilepage.Size.Height;
            Settings.Default.Save();
        }
    }
}
