﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex03.GarageLogic
{
    public class ElectricMotorcycle : Motorcycle
    {
        public ElectricMotorcycle(
           List<string> LicenseTypeAllowedToDrive,
            string ModelName,
            string LicenseNumber,
            int numbersOfWheels,
            string m_ManufacturerName,
            float m_CurrentAirPressure,
            float m_MaxAirPressure, float CurrAmountTank, ConditionInTheGarage i_ConditionInTheGarage, string m_OwnersName, string i_PhoneName)
            : base(LicenseTypeAllowedToDrive, ModelName, LicenseNumber, 
                  numbersOfWheels, m_ManufacturerName, m_CurrentAirPressure, 
                  m_MaxAirPressure, FuelTypes.Electric, 1.8f, CurrAmountTank, ConditionInTheGarage.InRepair,  m_OwnersName,  i_PhoneName)
        {}

        public override void FillTank(float numberOfHoursToChargeTheBattery, FuelTypes i_FuelType = FuelTypes.Electric)
        {
            if (i_FuelType != this.m_FuelType)
            {
                return;
            }
            float curr_amount = this.get_m_CurrAmountTank();
            if ((this.m_CapacityOfCargo - curr_amount) > numberOfHoursToChargeTheBattery)
            {
                set_m_CurrAmountTank(curr_amount + numberOfHoursToChargeTheBattery);
            }
        }
    }
}
