﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex03.GarageLogic
{
    public abstract class Car : Vehicle
    {
        private NumOfDoors m_NumOfDoors;
        private Color m_Color;
        
        public Car(NumOfDoors numOfDoors, Color color, string ModelName, 
            string LicenseNumber, int numbersOfWheels, string m_ManufacturerName, 
            float m_CurrentAirPressure, float m_MaxAirPressure, FuelTypes m_FuelTypes, float m_CapacityOfCargo,
            float CurrAmountTank, ConditionInTheGarage i_ConditionInTheGarage, string m_OwnersName, string i_PhoneName)
            : base(ModelName, LicenseNumber, numbersOfWheels, m_ManufacturerName, 
                  m_CurrentAirPressure, m_MaxAirPressure, m_FuelTypes, m_CapacityOfCargo, CurrAmountTank, i_ConditionInTheGarage, m_OwnersName, i_PhoneName)
        {
            this.m_NumOfDoors = numOfDoors;
            this.m_Color = color;
        }
    }


    public enum Color
    {
        Gray = 1,
        blue = 2,
        white = 3,
        black = 4
    }
    public enum NumOfDoors
    {
        two = 2,
        three = 3,
        four = 4,
        five = 5
    }
}
