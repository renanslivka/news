﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex03.GarageLogic
{
    public abstract class Vehicle
    {
        public string m_ModelName;
        public string m_LicenseNumber;
        public List<Wheel> wheels;
        public FuelTypes m_FuelType;
        public readonly float m_CapacityOfCargo;
        private float m_CurrAmountTank;
        public ConditionInTheGarage m_ConditionInTheGarage;
        public string m_OwnersName;
        public string m_PhoneName;

        public Vehicle(string ModelName, string LicenseNumber, int numbersOfWheels, 
            string m_ManufacturerName, float m_CurrentAirPressure, float m_MaxAirPressure, FuelTypes i_FullType,
            float CapacityOfCargo, float CurrAmountTank, ConditionInTheGarage i_ConditionInTheGarage, string i_OwnersName, string i_PhoneName)
        {
            this.m_ModelName = ModelName;
            this.m_LicenseNumber = LicenseNumber;
            this.m_FuelType = i_FullType;
            this.m_CapacityOfCargo = CapacityOfCargo;
            this.set_m_CurrAmountTank(CurrAmountTank);
            this.m_ConditionInTheGarage = i_ConditionInTheGarage;
            this.m_OwnersName = i_OwnersName;
            this.m_PhoneName = i_PhoneName;

            if (wheels is null)
            {
                wheels = new List<Wheel>();                
            }
            for (int i=0; i < numbersOfWheels; i++)
            {
                this.wheels.Add(new Wheel(m_ManufacturerName, m_CurrentAirPressure, m_MaxAirPressure));
            }
        }

        public float get_m_CurrAmountTank()
        {
            return this.m_CurrAmountTank;
        }

        public void set_m_CurrAmountTank(float amount)
        {
            this.m_CurrAmountTank = amount;
        }

        public float get_PercentOfEnergyLeft()
        {
            return 100*( this.m_CurrAmountTank / this.m_CapacityOfCargo);
        }
        
        public virtual void FillTank(float AmountToAddToTank, FuelTypes full_type) { }

    }
    public enum FuelTypes
    {
        Octan95 = 1,
        Octan96 = 2,
        Octan98 = 3,
        Soler = 4,
        Electric = 5
    }

    public enum ConditionInTheGarage
    {
        InRepair = 1,
        Repaired = 2,
        Paid = 3
    }
}
