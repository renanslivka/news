﻿using Ex03.GarageLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex03.ConsoleUI
{
    public class Program
    {
        public static void Main()
        {
            Garage garage = new Garage();
            List<string> ValidInpuuts = new List<string>() { "1", "2", "3", "4", "5", "6", "7" };
            List<string> AllInputs = new List<string>() { "1", "2", "3", "4", "5", "6", "7" };
            string m_Input = "INIT";
            while (m_Input != "7" && (m_Input == "INIT" || AllInputs.Contains(m_Input)))
            {
                Console.WriteLine(
                  @"Hello and welcom to Garage system!!!
what is your wish:
1. Enter your vehicle to the garage 
2. Want to see the list of the License Number of the vehicles wich inside the garage currently
3. Want to change status of vehicle
4. Want to inflate air in the wheels
5. Refuel/Recharge vehicle
6. Want to see all the ditail on specific vehicle
7. Exit");

                m_Input = Console.ReadLine();

                while (!AllInputs.Contains(m_Input))
                {
                    Console.WriteLine("Wrong input - Please try again");
                    m_Input = Console.ReadLine();
                }


                switch (m_Input)
                {
                    case "1":
                        CreateChoosenVehicle(garage);
                        break;

                    case "2":
                        PrintTheLicenseNumberOfVehicleInGarage(garage);
                        break;

                    case "3":
                        ChangeVehicleConditionInTheGarage(garage);
                        break;

                    case "4":
                        ChoosenVehicleForInflateHisWheel(garage);
                        break;

                    case "5":
                        ChooseVehicleForFillIsTank(garage);
                        break;

                    case "6":
                        PrintTheAllDetailsOfVehicleInGarage(garage);
                        break;
                }
            }
        }

        public static void CreateChoosenVehicle(Garage garage)
        {
            List<string> ValidInpuuts = new List<string>() { "1", "2", "3", "4" };
            string m_Input = "INIT";
            if (m_Input != "4" && (m_Input == "INIT" || ValidInpuuts.Contains(m_Input)))
            {
                Console.WriteLine(
                  @"
what is your wish:
Vehivle v:
1. car    
2. motorcyclee
3. track      
4. Exit");
                m_Input = Console.ReadLine();

                while (!ValidInpuuts.Contains(m_Input))
                {
                    Console.WriteLine("Wrong input - Please try again");
                    m_Input = Console.ReadLine();
                }


                switch (m_Input)
                {
                    case "1":
                        ChooseWhichCarToCreate(garage);
                        break;

                    case "2":
                        ChooseWhichMotorcycleToCreate(garage);
                        break;

                    case "3":
                        InitFuelTruckToAdd(garage);
                        break;
                }
            }
        }

        public static void InitFuelTruckToAdd(Garage garage)
        {
            Console.WriteLine("Enter Owner Name of the vhicle :");
            string m_OwnersName = Console.ReadLine();
            Console.WriteLine("What is the phone number of the contact person:");
            string m_PhoneNumber = Console.ReadLine();
            Console.WriteLine("Is The Trunk Cooled? True/False");
            bool IsTheTrunkCooled = bool.Parse(Console.ReadLine());
            Console.WriteLine("What is Model Name: ");
            string ModelName = Console.ReadLine();
            Console.WriteLine("Enter License Number:");
            string LicenseNumber = Console.ReadLine();
            Console.WriteLine("What is the Manufacturer Name of the wheel: ");
            string m_ManufacturerName = Console.ReadLine();
            Console.WriteLine("What is the Current Air Pressure of the wheel:");
            float m_CurrentAirPressure = float.Parse(Console.ReadLine());
            Console.WriteLine("What is the Current Amount Tank:");
            float CurrAmountTank = float.Parse(Console.ReadLine());

            bool isTheVehicleAlreadyInGarage = garage.CreateFuelTruckToAdd(IsTheTrunkCooled, ModelName, LicenseNumber, 12,
                m_ManufacturerName, m_CurrentAirPressure, 28, CurrAmountTank, m_OwnersName, m_PhoneNumber);
            if (isTheVehicleAlreadyInGarage)
            {
                Console.WriteLine("The vehicle is already inside the garage");
            }

        }

        public static void ChooseWhichCarToCreate(Garage garage)
        {
            // get from the user what he wants to add
            // 1. fuel car            -> createFuelCarToAdd()
            // 2. electric car    -> createElectricCarToAdd()
            List<string> ValidInpuuts = new List<string>() { "1", "2", "3" };
            string m_Input = "INIT";
            if (m_Input != "4" && (m_Input == "INIT" || ValidInpuuts.Contains(m_Input)))
            {
                Console.WriteLine(
                  @"
what is your wish:
1. Add Fuel Car     
2. Add Electric Car
3. Exit");

                m_Input = Console.ReadLine();

                while (!ValidInpuuts.Contains(m_Input))
                {
                    Console.WriteLine("Wrong input - Please try again");
                    m_Input = Console.ReadLine();
                }


                switch (m_Input)
                {
                    case "1":
                        InitFuelCarToAdd(garage);
                        break;

                    case "2":
                        InitElectricCarToAdd(garage);
                        break;
                }
            }
        }

        public static void InitFuelCarToAdd(Garage garage)
        {
            Console.WriteLine("Enter Owner Name of the vhicle :");
            string m_OwnersName = Console.ReadLine();
            Console.WriteLine("What is the phone number of the contact person:");
            string m_PhoneNumber = Console.ReadLine();

            Console.WriteLine("Enter Number of doors you have in you car: ");

            bool isNumber = int.TryParse(Console.ReadLine(), out int numOfDoors);// Checks if the input is a number        
            while (NumOfDoors.two != (NumOfDoors)numOfDoors && NumOfDoors.three != (NumOfDoors)numOfDoors && // Checks whether the number of doors equals the number of doors you can enter
                NumOfDoors.four != (NumOfDoors)numOfDoors && NumOfDoors.five != (NumOfDoors)numOfDoors && isNumber)
            {
                Console.WriteLine("Invalide Output\n Press 2,3,4,5 for valid output");
                numOfDoors = int.Parse(Console.ReadLine());
            }

            Console.WriteLine(@"Enter the Color car: 
1.Gray
2.blue
3.white
4.black ");
            isNumber = int.TryParse(Console.ReadLine(), out int color);// Checks if the input is a number
            while (Color.black != (Color)color && Color.blue != (Color)color && Color.Gray != (Color)color && Color.white != (Color)color && isNumber)
            {
                Console.WriteLine(@"Invalide Output\n Press 1.Gray
2.blue
3.white
4.black for valid output");
                color = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("What is Model Name: ");
            string ModelName = Console.ReadLine();
            Console.WriteLine("Enter License Number:");
            string LicenseNumber = Console.ReadLine();
            Console.WriteLine("What is the Manufacturer Name of the wheel: ");
            string m_ManufacturerName = Console.ReadLine();
            Console.WriteLine("What is the Current Air Pressure of the wheel:");
            float m_CurrentAirPressure = float.Parse(Console.ReadLine());
            Console.WriteLine("What is the Current Amount Tank:");
            float CurrAmountTank = float.Parse(Console.ReadLine());

            bool isTheVehicleAlreadyInGarage = garage.CreateFuelCarToAdd((NumOfDoors)numOfDoors, (Color)color, ModelName, LicenseNumber, 4, m_ManufacturerName,
                m_CurrentAirPressure, 32, CurrAmountTank, m_OwnersName, m_PhoneNumber);
            if (isTheVehicleAlreadyInGarage)
            {
                Console.WriteLine("The vehicle is already inside the garage");
            }
        }

        public static void InitElectricCarToAdd(Garage garage)
        {
            Console.WriteLine("Enter Owner Name of the vhicle :");
            string m_OwnersName = Console.ReadLine();
            Console.WriteLine("What is the phone number of the contact person:");
            string m_PhoneNumber = Console.ReadLine();

            Console.WriteLine("Enter Number of doors you have in you car: ");

            bool isNumber = int.TryParse(Console.ReadLine(), out int numOfDoors);// Checks if the input is a number        
            while (NumOfDoors.two != (NumOfDoors)numOfDoors && NumOfDoors.three != (NumOfDoors)numOfDoors && // Checks whether the number of doors equals the number of doors you can enter
                NumOfDoors.four != (NumOfDoors)numOfDoors && NumOfDoors.five != (NumOfDoors)numOfDoors && isNumber)
            {
                Console.WriteLine("Invalide Output\n Press 2,3,4,5 for valid output");
                numOfDoors = int.Parse(Console.ReadLine());
            }

            Console.WriteLine(@"Enter the Color car: 
1.Gray
2.blue
3.white
4.black ");
            isNumber = int.TryParse(Console.ReadLine(), out int color);// Checks if the input is a number
            while (Color.black != (Color)color && Color.blue != (Color)color && Color.Gray != (Color)color && Color.white != (Color)color && isNumber)
            {
                Console.WriteLine(@"Invalide Output\n Press 1.Gray
2.blue
3.white
4.black for valid output");
                color = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("What is Model Name: ");
            string ModelName = Console.ReadLine();
            Console.WriteLine("Enter License Number:");
            string LicenseNumber = Console.ReadLine();
            Console.WriteLine("What is the Manufacturer Name of the wheel: ");
            string m_ManufacturerName = Console.ReadLine();
            Console.WriteLine("What is the Current Air Pressure of the wheel:");
            float m_CurrentAirPressure = float.Parse(Console.ReadLine());
            Console.WriteLine("What is the Current Amount Tank:");
            float CurrAmountTank = float.Parse(Console.ReadLine());


            bool isTheVehicleAlreadyInGarage = garage.CreateElectricCarToAdd((NumOfDoors)numOfDoors, (Color)color, ModelName, LicenseNumber, 4, m_ManufacturerName,
                m_CurrentAirPressure, 32, CurrAmountTank, m_OwnersName, m_PhoneNumber);
            if (isTheVehicleAlreadyInGarage)
            {
                Console.WriteLine("The vehicle is already inside the garage");
            }
        }
        //------------------------------------------------------------------
        public static void ChooseWhichMotorcycleToCreate(Garage garage)
        {
            List<string> ValidInpuuts = new List<string>() { "1", "2", "3" };
            string m_Input = "INIT";
            if (m_Input != "4" && (m_Input == "INIT" || ValidInpuuts.Contains(m_Input)))
            {
                Console.WriteLine(
                  @"
what is your wish:
1. Add Fuel Motorcycle     
2. Add Electric Motorcycle
3. Exit");

                m_Input = Console.ReadLine();

                while (!ValidInpuuts.Contains(m_Input))
                {
                    Console.WriteLine("Wrong input - Please try again");
                    m_Input = Console.ReadLine();
                }


                switch (m_Input)
                {
                    case "1":
                        InitFuelMotorcycleToAdd(garage);
                        break;

                    case "2":
                        InitElectricMotorcycleToAdd(garage);
                        break;
                }
            }
        }

        public static void InitFuelMotorcycleToAdd(Garage garage)
        {
            Console.WriteLine("Enter Owner Name of the vhicle :");
            string m_OwnersName = Console.ReadLine();
            Console.WriteLine("What is the phone number of the contact person:");
            string m_PhoneNumber = Console.ReadLine();

            Console.WriteLine("What is Model Name: ");
            string ModelName = Console.ReadLine();
            Console.WriteLine("Enter License Number:");
            string LicenseNumber = Console.ReadLine();
            Console.WriteLine("What is the Manufacturer Name of the wheel: ");
            string m_ManufacturerName = Console.ReadLine();
            Console.WriteLine("What is the Current Air Pressure of the wheel:");
            float m_CurrentAirPressure = float.Parse(Console.ReadLine());
            Console.WriteLine("What is the Current Amount Tank:");
            float CurrAmountTank = float.Parse(Console.ReadLine());            

            List<string> LicenseTypeAllowedToDrive = new List<string>();// No matter what license the user has, I am interested in what kind of license it is permissible to drive the motorcycle in general
            bool isTheVehicleAlreadyInGarage = garage.CreateElectricMotorcycleToAdd(LicenseTypeAllowedToDrive,
            ModelName, LicenseNumber, 2, m_ManufacturerName, m_CurrentAirPressure, 30, CurrAmountTank, m_OwnersName, m_PhoneNumber);

            if (isTheVehicleAlreadyInGarage)
            {
                Console.WriteLine("The vehicle is already inside the garage");
            }
        }

        public static void InitElectricMotorcycleToAdd(Garage garage)
        {
            Console.WriteLine("Enter Owner Name of the vhicle :");
            string m_OwnersName = Console.ReadLine();
            Console.WriteLine("What is the phone number of the contact person:");
            string m_PhoneNumber = Console.ReadLine();
            Console.WriteLine("What is Model Name: ");
            string ModelName = Console.ReadLine();
            Console.WriteLine("Enter License Number:");
            string LicenseNumber = Console.ReadLine();
            Console.WriteLine("What is the Manufacturer Name of the wheel: ");
            string m_ManufacturerName = Console.ReadLine();
            Console.WriteLine("What is the Current Air Pressure of the wheel:");
            float m_CurrentAirPressure = float.Parse(Console.ReadLine());
            Console.WriteLine("What is the Current Amount Tank:");
            float CurrAmountTank = float.Parse(Console.ReadLine());

            List<string> LicenseTypeAllowedToDrive = new List<string>();// No matter what license the user has, I am interested in what kind of license it is permissible to drive the motorcycle in general
            bool isTheVehicleAlreadyInGarage = garage.CreateElectricMotorcycleToAdd(LicenseTypeAllowedToDrive,
                ModelName, LicenseNumber, 2, m_ManufacturerName, m_CurrentAirPressure, 30, CurrAmountTank, m_OwnersName, m_PhoneNumber);

            if (isTheVehicleAlreadyInGarage)
            {
                Console.WriteLine("The vehicle is already inside the garage");
            }
        }
        //------------------------------------------------------------------
        public static void PrintTheLicenseNumberOfVehicleInGarage(Garage garage)
        {
            if (garage.VehicleInGarage.Count != 0)
            {
                List<string> ValidInpuuts = new List<string>() { "1", "2", "3", "4" };
                string m_Input = "INIT";
                if (m_Input != "5" && (m_Input == "INIT" || ValidInpuuts.Contains(m_Input)))
                {
                    Console.WriteLine(
                      @"
what is your wish:
1. print all the License Number of the vehicle which InRepair            
2.print all the License Number of the vehicle which Repaired
3.print all the License Number of the vehicle which Paid 
4. print all the License Number of the vehicle in garage
5. Exit");

                    m_Input = Console.ReadLine();

                    while (!ValidInpuuts.Contains(m_Input))
                    {
                        Console.WriteLine("Wrong input - Please try again");
                        m_Input = Console.ReadLine();
                    }


                    switch (m_Input)
                    {
                        case "1":
                            foreach (Vehicle vehicle in garage.VehicleInGarage)
                            {
                                if (vehicle.m_ConditionInTheGarage == ConditionInTheGarage.InRepair)
                                {
                                    Console.WriteLine(vehicle.m_LicenseNumber);
                                }
                            }
                            break;

                        case "2":
                            foreach (Vehicle vehicle in garage.VehicleInGarage)
                            {
                                if (vehicle.m_ConditionInTheGarage == ConditionInTheGarage.Repaired)
                                {
                                    Console.WriteLine(vehicle.m_LicenseNumber);
                                }
                            }
                            break;
                        case "3":

                            foreach (Vehicle vehicle in garage.VehicleInGarage)
                            {
                                if (vehicle.m_ConditionInTheGarage == ConditionInTheGarage.Paid)
                                {
                                    Console.WriteLine(vehicle.m_LicenseNumber);
                                }
                            }
                            break;
                        case "4":

                            foreach (Vehicle vehicle in garage.VehicleInGarage)
                            {
                                Console.WriteLine(vehicle.m_LicenseNumber);
                            }
                            break;
                    }
                }
            }
            else
            {
                Console.WriteLine("There are no vehicles in the garage!! \n");
            }
        } // Option number 2

        public static void ChangeVehicleConditionInTheGarage(Garage garage)
        {
            string Input = Console.ReadLine();
            foreach (Vehicle vehicle in garage.VehicleInGarage)
            {
                if (Input == vehicle.m_LicenseNumber)
                {
                    List<string> ValidInpuuts = new List<string>() { "1", "2", "3" };
                    string m_Input = "INIT";
                    if (m_Input != "4" && (m_Input == "INIT" || ValidInpuuts.Contains(m_Input)))
                    {
                        Console.WriteLine(
                          @"
what is your wish:
1. Change the vehicle condition to InRepair     
2. Change the vehicle condition to Repaired
3. Change the vehicle condition to Paid
4. Exit");

                        m_Input = Console.ReadLine();

                        while (!ValidInpuuts.Contains(m_Input))
                        {
                            Console.WriteLine("Wrong input - Please try again");
                            m_Input = Console.ReadLine();
                        }


                        switch (m_Input)
                        {
                            case "1":
                                vehicle.m_ConditionInTheGarage = ConditionInTheGarage.InRepair;
                                break;

                            case "2":
                                vehicle.m_ConditionInTheGarage = ConditionInTheGarage.Repaired;
                                break;
                            case "3":
                                vehicle.m_ConditionInTheGarage = ConditionInTheGarage.Paid;
                                break;
                        }
                    }
                }
            }
        } // Option number 3

        public static void ChoosenVehicleForInflateHisWheel(Garage garage)
        {
            if (garage.VehicleInGarage.Capacity == 0)
            {
                Console.WriteLine(@"There is no vehicle in the garage!,
therfore there is no possibillity to inflate the wheels! 
");
            }
            else
            {
                Console.WriteLine("Enter License Number of the vehicle you wish to inflate his wheels: ");
                string LicenseNumber = Console.ReadLine();
                foreach (Vehicle vehicle in garage.VehicleInGarage)
                {
                    if (vehicle.m_LicenseNumber == LicenseNumber)
                    {
                        for (int i = 0; i < garage.VehicleInGarage.Count; i++)
                        {
                            for (int j = 0; j < vehicle.wheels.Count; j++)
                                Wheel.Inflate(garage.VehicleInGarage[i].wheels[j].m_CurrentAirPressure, garage.VehicleInGarage[i].wheels[j].m_MaxAirPressure);
                        }
                        Console.WriteLine("The wheels of your vehicle have been inflated!!");
                    }
                }
            }
        } // Option number 4

        public static void ChooseVehicleForFillIsTank(Garage garage)
        {
            if (garage.VehicleInGarage.Count != 0)
            {
                Console.WriteLine("Enter License Number of the vehicle you wish to fill his tank: ");
                string LicenseNumber = Console.ReadLine();
                foreach (Vehicle vehicle in garage.VehicleInGarage)
                {
                    if (vehicle.m_LicenseNumber == LicenseNumber)
                    {
                        Console.WriteLine("Enter Amount to add the tank: ");
                        float AmountToAddTheTank = float.Parse(Console.ReadLine());
                        while ((vehicle.m_CapacityOfCargo - vehicle.get_m_CurrAmountTank()) < AmountToAddTheTank)
                        {
                            Console.WriteLine("Please type data again! your input Go through the {0} maximum possible capacity ", vehicle.m_CapacityOfCargo);
                            AmountToAddTheTank = float.Parse(Console.ReadLine());
                        }
                        Console.WriteLine("Now your input is correctly");
                        vehicle.FillTank(AmountToAddTheTank, vehicle.m_FuelType);
                        Console.WriteLine("Your vehicle is now filled up with the amount you wish for");
                    }
                    else
                    {
                        Console.WriteLine("There is no vehicle with this License Number in the garage! ");
                    }
                }
            }
            else
            {
                Console.WriteLine("There is no vehicle in the garage! ");
            }
        } // Option number 5

        public static void PrintTheAllDetailsOfVehicleInGarage(Garage garage) // Option number 6
        {
            if (garage.VehicleInGarage.Count != 0)
            {
                 Console.WriteLine("Enter License Number of the vehicle you wish to see is details: ");
                string LicenseNumber = Console.ReadLine();
                foreach (Vehicle vehicle in garage.VehicleInGarage)
                {
                    if (vehicle.m_LicenseNumber == LicenseNumber)
                    {
                        Console.WriteLine("----------------------------------------------------");
                        Console.WriteLine("License Number: {0}", vehicle.m_LicenseNumber);
                        Console.WriteLine("Model Name: {0}", vehicle.m_ModelName);
                        Console.WriteLine("Owner Name: {0}", vehicle.m_OwnersName);
                        Console.WriteLine("Condition in the garage: {0}", vehicle.m_ConditionInTheGarage);
                        Console.WriteLine("Curr Amount Tank: {0}", vehicle.get_m_CurrAmountTank());
                        Console.WriteLine("Capacity Of Cargo: {0}", vehicle.m_CapacityOfCargo);
                        Console.WriteLine("Percent Of Energy Left: {0}", vehicle.get_PercentOfEnergyLeft());
                        Console.WriteLine("FuelType: {0}", vehicle.m_FuelType);
                        Console.WriteLine("Manufacturer Name of the wheels: {0}", vehicle.wheels[0].m_ManufacturerName);
                        for (int i = 0; i < garage.VehicleInGarage.Count; i++)
                        {
                            for (int j = 0; j < vehicle.wheels.Count; j++)
                            {
                                Console.WriteLine("Current Air Pressure of wheel number {0}: {1}", j + 1, garage.VehicleInGarage[i].wheels[j].m_CurrentAirPressure);
                            }
                            Console.WriteLine("----------------------------------------------------");
                        }
                    }
                }
                Console.WriteLine("");
            }
            else
            {
                Console.WriteLine("There are no vehicles in the garage!! \n");
            }
        }
    }
}

